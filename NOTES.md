# Notes

- https://github.com/joaopalmeiro/template-ts-package
- https://gitlab.com/joaommpalmeiro/tokenss
- https://gitlab.com/joaommpalmeiro/resetss
- [CSS Plugin default transform should expose the same API and params with the custom one](https://github.com/terrazzoapp/terrazzo/issues/145) issue
- https://github.com/terrazzoapp/terrazzo/blob/030d2079affbbbf7a7bfa466591ae2a8970df491/packages/token-tools/src/css/lib.ts#L43
- https://cobalt-ui.pages.dev/advanced/node:
  - "Cobalt's Node.js API is for parsing and validating the Design Tokens Community Group format (DTCG) standard."
  - "It can't output code like the CLI can, but it is a lightweight and fast parser/validator for the DTCG spec that could even be used in client code if desired."
- https://terrazzo.app/docs/cli/config/
- https://terrazzo.app/docs/cli/integrations/css/#config
- https://www.npmjs.com/package/@terrazzo/plugin-css
- https://www.npmjs.com/package/@terrazzo/cli

## Commands

```bash
rm -rf node_modules/ && npm install
```

```bash
npm install -D @terrazzo/cli @terrazzo/plugin-css
```

## Snippets

### `config/pico.tokens.config.mjs` file

```js
import pluginCSS from "@cobalt-ui/plugin-css";

/** @type {import("@cobalt-ui/core").Config} */
export default {
  plugins: [
    pluginCSS({
      p3: false,
      colorFormat: "none",
      filename: "./pico.tokens.css",
      prefix: "pico",
    }),
  ],
  tokens: ["tokenss/dtcg/pico.tokens.json"],
  outDir: "../src",
};
```

### `package.json` file

```json
{
  "scripts": {
    "build": "co build --config ./config/pico.tokens.config.mjs",
    "format": "sort-package-json",
    "lint": "sort-package-json --check"
  },
  "devDependencies": {
    "@cobalt-ui/cli": "1.11.2",
    "@cobalt-ui/plugin-css": "1.7.4",
    "sort-package-json": "2.10.0",
    "tokenss": "0.1.0"
  }
}
```
